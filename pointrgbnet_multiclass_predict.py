#!/usr/bin/env python

import os
import numpy as np
import re
import tensorflow as tf
import keras
import keras.backend as K
from keras.optimizers import Adam
import sys
import subprocess
import cv2

from keras.backend.tensorflow_backend import set_session
config = tf.ConfigProto()
config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
sess = tf.Session(config=config)
set_session(sess)  # set this TensorFlow session as the default session for Keras
graph = tf.get_default_graph()
sys.path.append("pointrgbnet")
import pointrgbnet


def parseDataXFile(dataXFile):
	datax = []
	f = open(dataXFile, 'r')
	for line in f:
		datax.append(float(line.split('\n')[0]))
	datax = np.array(datax).reshape(1, 20, 20, 20, 1)
	return datax


def natural_sort(l):
	convert = lambda text: int(text) if text.isdigit() else text.lower() 
	alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
	return sorted(l, key = alphanum_key)


def getListOfFiles(path, ext):
	files = []
	files = [ path + entry for entry in os.listdir(path) if os.path.isfile(os.path.join(path, entry)) and ext in entry ]
	files = natural_sort(files)
	return files



def runSamples():
	pathToModel = "/home/fran/Documentos/pointrgbnet_multiclass/snapshots/fs_new_arch_m_52.h5"
	full_model, logits2d_model = pointrgbnet.PointRgbNet()
	full_model.load_weights(pathToModel)
	opt = Adam(lr=0.000001)
	
	full_model.compile(optimizer=opt, metrics=['accuracy'], loss="categorical_crossentropy" )


	data_2d = getListOfFiles("data/", ".jpg")
	data_3d = getListOfFiles("data/", ".pcd")


	for d_2d, d_3d in zip(data_2d, data_3d):

		print d_2d
		print d_3d
		ret = subprocess.call(['voxelizer/build/voxelizer', d_3d, "tmp.occ"])
		
		occ_data = parseDataXFile("tmp.occ")

		img_data_ori = cv2.imread(d_2d)
		img_data = cv2.resize(img_data_ori, (150, 200))
		img_data = img_data.reshape(1, 200,150,3)


		with graph.as_default():
			r = full_model.predict(
				x=[occ_data, img_data]
			)

		cat = r.argmax() # 0 pedestrian, 1 vehicle, 2 background

		if cat == 0: print "Pedestrian"
		if cat == 1: print "Vehicle"
		if cat == 2: print "Background/Not interesting object"

		def visualizers():
			cv2.imshow("vis",img_data_ori)
			cv2.waitKey(100)
			ret = subprocess.call(['pcl_viewer', d_3d])

		visualizers()

		




if __name__== "__main__":
  runSamples()