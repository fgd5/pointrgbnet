from keras.layers import merge, Input
from keras.layers import Dense, Activation, Flatten
from keras.layers import Conv3D
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Dropout
from keras.layers import Concatenate
from keras.models import Model
from keras.preprocessing import image
import keras.backend as K
from keras.utils.layer_utils import convert_all_kernels_in_model
from keras.utils.data_utils import get_file
from keras.optimizers import Adam




def build_pointnet(inputs):

    x = Conv3D(100, (3,3,3), strides=(1, 1, 1), padding='valid', activation='relu', use_bias=True, kernel_initializer='glorot_uniform', trainable=True)(inputs)
    x = Dropout(0.5)(x)
    x = Conv3D(100, (2,2,2), strides=(1, 1, 1), padding='valid', activation='relu', use_bias=True, kernel_initializer='glorot_uniform', trainable=True)(x)
    x = Conv3D(64, (2,2,2), strides=(1, 1, 1), padding='valid', activation='relu', use_bias=True, kernel_initializer='glorot_uniform', trainable=True)(x)
    x = Dropout(0.5)(x)
    x = Flatten(name="logits_3d")(x)

    return x


def build_alexnet(inputs):

	x = Conv2D(filters=96, kernel_size=(11,11), strides=(4,4), padding='valid', activation='relu')(inputs)
	x = MaxPooling2D(pool_size=(2,2), strides=(2,2), padding='valid')(x)
	x = Conv2D(filters=256, kernel_size=(11,11), strides=(1,1), padding='valid', activation='relu')(x)
	x = MaxPooling2D(pool_size=(2,2), strides=(2,2), padding='valid')(x)
	x = Conv2D(filters=384, kernel_size=(3,3), strides=(1,1), padding='valid', activation='relu')(x)
	x = Dropout(0.5)(x)
	x = Flatten(name="logits_2d")(x)

	return x


def PointRgbNet():

    point_input = Input(shape=(20,20,20,1))
    rgb_input = Input(shape=(200,150,3))
    conv_3d = build_pointnet(point_input)
    conv_2d = build_alexnet(rgb_input)
    combined = Concatenate()([conv_3d, conv_2d])
    category_output = Dense(3, activation="softmax")(combined)

    full_model = Model(
            inputs=[point_input, rgb_input],
            outputs=category_output,
            name="pointrgbnet"
    )

    logits2d_model = Model(
            inputs=[point_input, rgb_input],
            outputs=full_model.get_layer("logits_2d").output,
            name="pointrgbnet_2dlogits"
    )

    return full_model, logits2d_model
